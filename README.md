Moved to GitHub under the [lucify-toolbox repo](https://github.com/lucified/lucify-toolbox). 
Remove this repo from your computer and check out the GitHub one.