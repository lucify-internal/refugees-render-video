
var gulp = require('gulp');
var builder = require('lucify-component-builder');
//var template = require('lucify-commons/src/js/lucify-page-def-template.js');

var opts = {

	pageDef: {
		title: "The flow towards Europe video render",
	},

	paths: [
		'node_modules/lucify-commons',
		'node_modules/lucify-refugees'
	],

	publishFromFolder: 'dist',
	defaultBucket: 'lucify-dev',
	maxAge: 60,
	assetContext: 'refugees-video-render/',
	baseUrl: 'http://dev.lucify.com/'
}

builder(gulp, opts);
