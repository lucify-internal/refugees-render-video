
require('phantomjs-polyfill');


var React = require('react');

var RefugeeMap = require('lucify-refugees/src/js/components/refugee-map/refugee-map.jsx');
var TimeLayer = require('lucify-refugees/src/js/components/refugee-map/refugee-map-time-layer.jsx');
var RefugeeContextDecorator = require('lucify-refugees/src/js/components/refugee-context-decorator.jsx');

var moment = require('moment');


var Main = React.createClass({


	componentWillMount: function() {
		this.mom = moment([2012, 0, 1]).add(0, 'hours');
	},


	componentDidMount: function() {
		//window.tick = this.tick;
		this.tickCount = 0;
		this.tick();

		window.tick = this.tick;
		window.tickTo = this.tickTo;
		window.tickSteps = this.tickSteps;
		// window.tick = function() {
		// 	var newMom = moment(this.mom).add('1', 'hours');
		// 	this.mom = newMom;
		// 	//console.log(newMom);
		// 	this.refs.rmap.updateForStamp(this.mom.unix());
		// 	this.refs.time.updateForStamp(this.mom.unix());
		// }.bind(this);
	},


	tickSteps: function(steps) {
		for (var i = 0; i < steps; i++) {
			var newMom = moment(this.mom).add(0.5, 'hours');
			this.mom = newMom;	
		}
	},


	tickTo: function(date) {
		this.mom = moment(date);
		this.refs.rmap.updateForStamp(this.mom.unix());
		this.refs.time.updateForStamp(this.mom.unix());
	},


	tick: function() {
		var newMom = moment(this.mom).add(0.5, 'hours');
		this.mom = newMom;
		this.tickCount++;
		console.log(this.tickCount);
		this.refs.rmap.updateForStamp(this.mom.unix());
		this.refs.time.updateForStamp(this.mom.unix());
		//requestAnimationFrame(this.tick);
		//window.setTimeout(this.tick, 200);   
	},


	render: function() {
		//console.log(this.mom.unix());
		return (
			<div className="main">
				<TimeLayer

				   ref="time"
				   stamp={this.mom.unix()}
				   {...this.props} />
			
				<RefugeeMap 
					lo={22.2206322}
					la={34.0485818 + 4.5}
					scale={0.50}
					width={2560}
					height={1440-100}
	 				ref="rmap"
	 				showDataUpdated={false}
	 				{...this.props}
	 				stamp={this.mom.unix()} />				
			</div>
		)
		
	}

});


module.exports = RefugeeContextDecorator(Main);

//module.exports = Main;


